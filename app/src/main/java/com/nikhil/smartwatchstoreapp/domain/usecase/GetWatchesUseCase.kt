package com.nikhil.smartwatchstoreapp.domain.usecase

import com.nikhil.smartwatchstoreapp.data.model.WatchList
import com.nikhil.smartwatchstoreapp.data.util.Resource
import com.nikhil.smartwatchstoreapp.domain.repository.WatchesRepository


class GetWatchesUseCase(private val getWatchesRepository: WatchesRepository) {

    suspend fun execute(): Resource<WatchList> {
        return getWatchesRepository.getWatches()
    }

}