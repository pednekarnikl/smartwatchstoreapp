package com.nikhil.smartwatchstoreapp.domain.repository

import com.nikhil.smartwatchstoreapp.data.util.Resource
import com.nikhil.smartwatchstoreapp.data.model.WatchList

interface WatchesRepository {

    suspend fun getWatches(): Resource<WatchList>

}
