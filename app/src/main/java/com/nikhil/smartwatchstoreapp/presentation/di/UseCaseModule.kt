package com.nikhil.smartwatchstoreapp.presentation.di

import com.nikhil.smartwatchstoreapp.domain.repository.WatchesRepository
import com.nikhil.smartwatchstoreapp.domain.usecase.GetWatchesUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Singleton
    @Provides
    fun provideWatchesUseCase(getWatchesRepository: WatchesRepository): GetWatchesUseCase {
        return GetWatchesUseCase(getWatchesRepository)
    }


}