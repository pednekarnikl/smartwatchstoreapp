package com.nikhil.smartwatchstoreapp.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nikhil.smartwatchstoreapp.domain.usecase.GetWatchesUseCase

class WatchesViewModelFactory(
    private val app: Application,
    private val getWatchesUseCase: GetWatchesUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return WatchesViewModel(
            app,
            getWatchesUseCase
        ) as T
    }

}