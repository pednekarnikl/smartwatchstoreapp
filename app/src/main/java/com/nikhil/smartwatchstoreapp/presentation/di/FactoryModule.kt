package com.nikhil.smartwatchstoreapp.presentation.di

import android.app.Application
import com.nikhil.smartwatchstoreapp.domain.usecase.GetWatchesUseCase
import com.nikhil.smartwatchstoreapp.presentation.viewmodel.WatchesViewModelFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class FactoryModule {

    @Singleton
    @Provides
    fun provideWatchesViewModelFactory(application: Application,
                                         getWatchesUseCase: GetWatchesUseCase
    ): WatchesViewModelFactory {
        return WatchesViewModelFactory(application,getWatchesUseCase)
    }



}