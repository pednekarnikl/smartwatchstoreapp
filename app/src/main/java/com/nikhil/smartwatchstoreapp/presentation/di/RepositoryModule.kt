package com.nikhil.smartwatchstoreapp.presentation.di

import com.nikhil.smartwatchstoreapp.data.repository.WatchesRepositoryImpl
import com.nikhil.smartwatchstoreapp.data.repository.dataSource.WatchesRemoteDataSource
import com.nikhil.smartwatchstoreapp.domain.repository.WatchesRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun provideWatchesRepository(watchesRemoteDataSource: WatchesRemoteDataSource
                                ): WatchesRepository {
        return WatchesRepositoryImpl(watchesRemoteDataSource)
    }



}