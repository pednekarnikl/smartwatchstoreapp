package com.nikhil.smartwatchstoreapp.presentation.di

import com.nikhil.smartwatchstoreapp.data.api.WatchAPIService
import com.nikhil.smartwatchstoreapp.data.repository.dataSource.WatchesRemoteDataSource
import com.nikhil.smartwatchstoreapp.data.repository.dataSourceImpl.WatchesRemoteDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

//24
@Module
@InstallIn(SingletonComponent::class)
class RemoteDataModule {

    @Singleton
    @Provides
    fun provideWatchesDataSource(watchAPIService: WatchAPIService): WatchesRemoteDataSource {
        return WatchesRemoteDataSourceImpl(watchAPIService)
    }



}