package com.nikhil.smartwatchstoreapp.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.denzcoskun.imageslider.models.SlideModel
import com.nikhil.smartwatchstoreapp.data.model.Watch
import com.nikhil.smartwatchstoreapp.databinding.WatchItemBinding

class WatchAdapter : RecyclerView.Adapter<WatchAdapter.WatchesViewHolder>() {

    //deffutil implementation calculates difference between two lists
    private val callback = object : DiffUtil.ItemCallback<Watch>(){
        override fun areItemsTheSame(oldItem: Watch, newItem: Watch): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Watch, newItem: Watch): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this,callback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WatchesViewHolder {
        val binding = WatchItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return WatchesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: WatchesViewHolder, position: Int) {
        val article = differ.currentList[position]
        holder.bind(article)
    }

    override fun getItemCount() = differ.currentList.size


   inner class WatchesViewHolder(val binding: WatchItemBinding):RecyclerView.ViewHolder(binding.root){
       fun bind(watch:Watch){
           binding.also {
               it.tvTitle.text = watch.title
               it.tvDescription.text = watch.description
               it.tvRatings.text = watch.rating.toString()
           }

           val imageList = ArrayList<SlideModel>()

           for(i in watch.images) imageList.add(SlideModel(i))

           binding.ivWatchImage.setImageList(imageList)


       }
   }
}