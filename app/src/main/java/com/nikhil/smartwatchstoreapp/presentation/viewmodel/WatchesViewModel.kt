package com.nikhil.smartwatchstoreapp.presentation.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.nikhil.smartwatchstoreapp.data.model.WatchList
import com.nikhil.smartwatchstoreapp.data.util.Resource
import com.nikhil.smartwatchstoreapp.domain.usecase.GetWatchesUseCase
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch


class WatchesViewModel(
    private val app: Application,
    private val getWatchesUseCase: GetWatchesUseCase
) : AndroidViewModel(app){

    val watches :MutableLiveData<Resource<WatchList>> = MutableLiveData()

    fun getWatchApi() = viewModelScope.launch(IO) {
        watches.postValue(Resource.Loading())
        try {
            if (isNetworkAvailable(app)) {
                val apiResult = getWatchesUseCase.execute()
                watches.postValue(apiResult)
            } else watches.postValue(Resource.Error("No Internet"))
        } catch (e:Exception){
            watches.postValue(Resource.Error(e.message.toString()))
        }

    }

    private fun isNetworkAvailable(context: Context?): Boolean {
        if (context == null) return false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        return true
                    }
                }
            }
        } else {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                return true
            }
        }
        return false

    }


}