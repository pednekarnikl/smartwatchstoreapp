package com.nikhil.smartwatchstoreapp.presentation

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class WatchApp():Application()

//all apps contain hilt must contain application class, ie annotated with hilt android app annotation
//this hilt android app annotation triggers hilt code generation