package com.nikhil.smartwatchstoreapp.presentation.di

import com.nikhil.smartwatchstoreapp.presentation.adapter.WatchAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AdapterModule {

    @Singleton
    @Provides
    fun provideWatchesAdapter(): WatchAdapter {
        return WatchAdapter()
    }



}