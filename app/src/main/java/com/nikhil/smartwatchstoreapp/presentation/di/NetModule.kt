package com.nikhil.smartwatchstoreapp.presentation.di

import com.nikhil.smartwatchstoreapp.BuildConfig
import com.nikhil.smartwatchstoreapp.data.api.WatchAPIService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

//21
@Module
@InstallIn(SingletonComponent::class)//as used throughout app we use application or SingletonComponent
class NetModule {

    @Singleton
    @Provides
    fun provideRetrofit():Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.BASE_URL)
            .build()
    }

    @Singleton
    @Provides
    fun provideNewsApiService(retrofit: Retrofit): WatchAPIService {
        return retrofit.create(WatchAPIService::class.java)
    }

}