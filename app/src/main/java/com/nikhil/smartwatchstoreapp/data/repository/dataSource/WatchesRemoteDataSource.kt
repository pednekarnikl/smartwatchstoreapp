package com.nikhil.smartwatchstoreapp.data.repository.dataSource

import com.nikhil.smartwatchstoreapp.data.model.WatchList
import retrofit2.Response

interface WatchesRemoteDataSource {
    suspend fun getWatches():Response<WatchList>
}