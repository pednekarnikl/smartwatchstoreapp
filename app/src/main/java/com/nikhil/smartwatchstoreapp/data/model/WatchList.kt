package com.nikhil.smartwatchstoreapp.data.model

data class WatchList(
    val limit: Int,
    val products: List<Watch>,
    val skip: Int,
    val total: Int
)