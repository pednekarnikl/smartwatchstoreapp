package com.nikhil.smartwatchstoreapp.data.repository

import com.nikhil.smartwatchstoreapp.data.util.Resource
import com.nikhil.smartwatchstoreapp.data.model.WatchList
import com.nikhil.smartwatchstoreapp.data.repository.dataSource.WatchesRemoteDataSource
import com.nikhil.smartwatchstoreapp.domain.repository.WatchesRepository
import retrofit2.Response

class WatchesRepositoryImpl(
    private val watchesRemoteDataSource: WatchesRemoteDataSource
): WatchesRepository {

    override suspend fun getWatches(): Resource<WatchList> {
        return responseToResource(watchesRemoteDataSource.getWatches())
    }

    private fun responseToResource(response: Response<WatchList>): Resource<WatchList> {
        if (response.isSuccessful){
            response.body()?.let {
                return Resource.Success(it)
            }
        }
        return Resource.Error("Error : ${response.message()}")
    }

}