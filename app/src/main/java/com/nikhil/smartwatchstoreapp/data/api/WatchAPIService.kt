package com.nikhil.smartwatchstoreapp.data.api

import com.nikhil.smartwatchstoreapp.data.model.WatchList
import retrofit2.Response
import retrofit2.http.GET

interface WatchAPIService {

    @GET("products")
    suspend fun getWatchesData(
//        @Query("language")
//        language:String/* = "en-US"*/,
//        @Query("page")
//        page:Int,
//        @Query("api_key")
//        apiKey:String = BuildConfig.API_KEY
    ): Response<WatchList>





}