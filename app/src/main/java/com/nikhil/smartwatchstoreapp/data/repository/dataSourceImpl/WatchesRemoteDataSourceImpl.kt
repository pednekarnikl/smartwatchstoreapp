package com.nikhil.smartwatchstoreapp.data.repository.dataSourceImpl

import com.nikhil.smartwatchstoreapp.data.api.WatchAPIService
import com.nikhil.smartwatchstoreapp.data.model.WatchList
import com.nikhil.smartwatchstoreapp.data.repository.dataSource.WatchesRemoteDataSource
import retrofit2.Response


class WatchesRemoteDataSourceImpl(
    private val watchAPIService: WatchAPIService
) : WatchesRemoteDataSource {

    override suspend fun getWatches(): Response<WatchList> {
        return watchAPIService.getWatchesData()
    }


}