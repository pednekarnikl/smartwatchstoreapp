package com.nikhil.smartwatchstoreapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.nikhil.smartwatchstoreapp.databinding.ActivityMainBinding
import com.nikhil.smartwatchstoreapp.presentation.adapter.WatchAdapter
import com.nikhil.smartwatchstoreapp.presentation.viewmodel.WatchesViewModel
import com.nikhil.smartwatchstoreapp.presentation.viewmodel.WatchesViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: WatchesViewModelFactory
    @Inject
    lateinit var watchAdapter: WatchAdapter
    lateinit var viewModel: WatchesViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(this,factory)[WatchesViewModel::class.java]

    }
}