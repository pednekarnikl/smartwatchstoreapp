package com.nikhil.smartwatchstoreapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.nikhil.smartwatchstoreapp.data.util.Resource
import com.nikhil.smartwatchstoreapp.databinding.FragmentWatchListBinding
import com.nikhil.smartwatchstoreapp.presentation.adapter.WatchAdapter
import com.nikhil.smartwatchstoreapp.presentation.viewmodel.WatchesViewModel


class WatchListFragment : Fragment() {

    private lateinit var watchAdapter: WatchAdapter
    private lateinit var viewModel: WatchesViewModel
    private lateinit var fragmentWatchListBinding: FragmentWatchListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_watch_list, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentWatchListBinding = FragmentWatchListBinding.bind(view)

        viewModel = (activity as MainActivity).viewModel
        watchAdapter = (activity as MainActivity).watchAdapter

        initRecyclerView()
        loadWatches()

    }

    private fun initRecyclerView() {
        fragmentWatchListBinding.rvWatch.also {
            it.adapter = watchAdapter
            it.layoutManager = LinearLayoutManager(activity)
        }
    }
    private fun toggleProgressbar(flag:Boolean){
        if (flag) fragmentWatchListBinding.pbWatches.visibility = View.VISIBLE else  fragmentWatchListBinding.pbWatches.visibility = View.INVISIBLE
    }

    private fun loadWatches() {
        viewModel.getWatchApi()
        viewModel.watches.observe(viewLifecycleOwner) { response ->
            when(response) {
                is Resource.Loading -> toggleProgressbar(true)
                is Resource.Error -> {
                    toggleProgressbar(false)
                    response.message?.let {
                        Toast.makeText(activity,"error : $it", Toast.LENGTH_LONG).show()
                    }
                }
                is Resource.Success -> {
                    response.data?.let {
                        toggleProgressbar(false)
                        Log.d("qwerty","watch list : ${Gson().toJson(it)}")
                        watchAdapter.differ.submitList(it.products.toList())
                    }
                }
            }
        }
    }


}